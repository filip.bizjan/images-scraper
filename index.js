const axios = require("axios")
const fs = require('fs');
const path = require('path');
const cron = require('cron');

const job = cron.CronJob.from({
	cronTime: '0 0 * * *', //every day at midnight
	onTick: function () {
		main()
	},
	start: true,
	timeZone: 'Europe/Ljubljana'
});

let urls = [
    "https://cdn.whatsupcams.com/snapshot/si_ankaran01.jpg",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/CERKN-JEZ_dir/siwc_CERKN-JEZ_ne.jpg",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/CERKN-JEZ_dir/siwc_CERKN-JEZ_s.jpg",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/CERKN-JEZ_dir/siwc_CERKN-JEZ_nw.jpg",
    "https://www.hribi.net/kamere/gradisce_pri_materiji_1614.jpg",
    "https://slike.hribi.net/kamere/rafut.jpg",
    "https://www.drsc.si/kamere/kamslike/divaca/slike/div1_0001.jpg",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/PORTOROZ_SECOVLJE_dir/siwc_PORTOROZ_SECOVLJE_n.jpg",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/PORTOROZ_SECOVLJE_dir/siwc_PORTOROZ_SECOVLJE_nw.jpg",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/PORTOROZ_SECOVLJE_dir/siwc_PORTOROZ_SECOVLJE_se.jpg",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/PORTOROZ_SECOVLJE_dir/siwc_PORTOROZ_SECOVLJE_s.jpg",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/PORTOROZ_SECOVLJE_dir/siwc_PORTOROZ_SECOVLJE_w.jpg",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/PORTOROZ_SECOVLJE_dir/siwc_PORTOROZ_SECOVLJE_e.jpg",
    "https://cdn.whatsupcams.com/snapshot/si_solkan02.jpg?hribi.net",
    "https://cdn.whatsupcams.com/snapshot/si_mostnasoci02.jpg?hribi.net",
    "https://cdn.whatsupcams.com/snapshot/si_crnikal01.jpg?hribi.net",
    "https://www.hribi.net/kamere/grmada_slavnik_4318.jpg",
    "https://cdn.whatsupcams.com/snapshot/si_koper04.jpg?hribi.net",
    "https://cdn.whatsupcams.com/snapshot/si_strunjan01.jpg?hribi.net",
    "https://www.hribi.net/kamere/kapunar_1072.jpg",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/MURSK-SOB_dir/siwc_MURSK-SOB_nw.jpg",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/MURSK-SOB_dir/siwc_MURSK-SOB_sw.jpg",
    "https://kamere.dars.si/kamere/Maribor/IP_34_L16.1_Pince.jpg",
    "https://cdn.whatsupcams.com/snapshot/si_rogla1.jpg?hribi.net",
    "https://cdn.whatsupcams.com/snapshot/si_rogla5.jpg?hribi.net",
    "https://cdn.whatsupcams.com/snapshot/si_rogla7.jpg?hribi.net",
    "https://cdn.whatsupcams.com/snapshot/si_maribor05.jpg?hribi.net",
    "https://cdn.whatsupcams.com/snapshot/si_ljgolf.jpg?hribi.net",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/LJUBL-ANA_BEZIGRAD_dir/siwc_LJUBL-ANA_BEZIGRAD_s.jpg",
    "https://cdn.whatsupcams.com/snapshot/si_medvode02.jpg?hribi.net",
    "https://cdn.whatsupcams.com/snapshot/si_medvode01.jpg?hribi.net",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/BOVEC_dir/siwc_BOVEC_ne_latest.jpg",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/BOVEC_dir/siwc_BOVEC_w.jpg",
    "https://cdn.whatsupcams.com/snapshot/si_mostnasoci03.jpg?hribi.net",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/MIKLAVZ_NA-GOR_dir/siwc_MIKLAVZ_NA-GOR_e.jpg",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/PLANINA_V-POD_dir/siwc_PLANINA_V-POD_e.jpg",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/PLANINA_V-POD_dir/siwc_PLANINA_V-POD_w.jpg",
    "https://cdn.whatsupcams.com/snapshot/si_hse4_2.jpg?hribi.net",
    "https://cdn.whatsupcams.com/snapshot/si_starivrh03.jpg?hribi.net",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/MARIBOR_SLIVNICA_dir/siwc_MARIBOR_SLIVNICA_se.jpg",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/MARIBOR_SLIVNICA_dir/siwc_MARIBOR_SLIVNICA_n.jpg",
    "https://meteo.arso.gov.si/uploads/probase/www/observ/webcam/MARIBOR_SLIVNICA_dir/siwc_MARIBOR_SLIVNICA_nw.jpg"
]
let names = [
    "ankaran_1", "cerknisko_jezero_ne", "cerknisko_jezero_s", "cerknisko_jezero_nw", "gradisce_pri_materiji", "rafut", "divaca", 
    "portotoz_secovlje_n", "portotoz_secovlje_nw", "portotoz_secovlje_se", "portotoz_secovlje_s", "portotoz_secovlje_w", "portotoz_secovlje_e",
    "solkan", "most_na_soci", "crni_kal", "grmada_slavnik", "koper", "strunjan", "kapunar", "murska_sobota_nw", "murska_sobota_sw",
    "maribor_pince", "rogla_1", "rogla_2", "rogla_3", "maribor", "lj_golf", "bezigrad_s", "medvode_1", "medvode_2", "bovec_ne", "bovec_w",
    "most_na_soci_2", "miklavz_na_gor", "planina_v_pod_e", "planina_v_pod_w", "hse", "stari_vrh", "maribor_slivnica_se", "maribor_slivnica_n", "maribor_slivnica_nw"
]

async function main(){
    let snapshotTimes = await getSnapshotTimes()
    console.log(`Scheduled times: ${snapshotTimes}`)

    setTimeout(async () => {
        await downloadFiles(1)
        console.log(`Snapshot 1 stored`)
    }, snapshotTimes[0] * 1000)

    setTimeout(async () => {
        await downloadFiles(2)
        console.log(`Snapshot 2 stored`)
    }, snapshotTimes[1] * 1000)

    setTimeout(async () => {
        await downloadFiles(3)
        console.log(`Snapshot 3 stored`)
    }, snapshotTimes[2] * 1000)
}

async function downloadFiles(snapshotIndex){
    let dir = `./files/${new Date().toLocaleString().split(',')[0].replaceAll('. ', "-").replaceAll("/", "-")}/${snapshotIndex}-${new Date().toLocaleString().split(',')[1].trim().replaceAll(":", "-").split(" ")[0]}`
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir, { recursive: true });
    }

    const downloadPromises = urls.map(async (url, index) => {
        try {
            const response = await axios({
                method: 'GET',
                url: url,
                responseType: 'stream'
            });

            const filePath = path.resolve(__dirname, dir, `file_${index}_${names[index]}.jpg`);
            const writer = fs.createWriteStream(filePath);

            response.data.pipe(writer);

            return new Promise((resolve, reject) => {
                writer.on('finish', resolve);
                writer.on('error', reject);
            });
        } catch (error) {
            console.error(`Error downloading file from ${url}: ${error}`);
            return null;
        }
    });

    return Promise.all(downloadPromises);
}

async function getSnapshotTimes(){
    let sunTime = (await axios.get("https://api.sunrisesunset.io/json?lat=46.056946&lng=14.505751")).data
    let { sunrise, sunset } = sunTime.results

    let minSeconds = timeToSeconds(sunrise)
    let maxSeconds = timeToSeconds(sunset)

    return generateRandomNumbers(minSeconds, maxSeconds, 7200) //at least 2 hours between snapshots
}

function timeToSeconds(timeStr) {
    const [time, modifier] = timeStr.split(' ');
    let [hours, minutes, seconds] = time.split(':').map(Number);

    if (hours === 12 && modifier === 'AM') {
        hours = 0;
    } else if (hours !== 12 && modifier === 'PM') {
        hours += 12;
    }

    return hours * 3600 + minutes * 60 + seconds;
}

function generateRandomNumbers(a, b, x) {
    let numbers = [];
    while (numbers.length < 3) {
        let randomNumber = Math.floor(Math.random() * (b - a + 1)) + a;

        if (numbers.every(num => Math.abs(num - randomNumber) >= x)) {
            numbers.push(randomNumber);
        }
    }

    return numbers;
}